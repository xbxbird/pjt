
#define DEBUG 1
#define numMeter 2
int led = 13;
char STDH[2][10]={
  "13A200","13A200"};
char STDL[2][10]={
  "40A04B03","40A04B03"};
char MTAD[2][15]={
  ":01 00000005",":01 00000005"};

char url[100]={
};
void setup() {
  // initialize serial: & set led pin as output
  pinMode(led, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  char ss[100]="S0123asdf456";
  // print the string
  ///answer = sendATXBee("+++","OK",500);
  //sprintf(ss100,"SFSD=%s",STDL[1]);
  //Serial.print("AT\n");
#ifdef DEBUG,
  Serial.print("Start");
#endif 
  runPending();
  while(1);
}
void runPending(){
  for(int i=0;i<numMeter;i++){
    delay(2000); 
    if(enterCMDXbee()==1){
      char atdl[30];   //at command 
      char atdh[30];    
#ifdef DEBUG
      Serial.print("Enter command mode  !!");
      Serial.println(i);
#endif  
      //change address
      sprintf(atdl,"ATDL=%s",STDL[i]);
      sprintf(atdh,"ATDH=%s",STDH[i]);
      sendATXBee(atdl,"OK",1000);
      sendATXBee(atdh,"OK",1000);
      sendATXBee("ATCN","OK",1000);
      //acquire meter 
      char buffer[100];
      GetMeter(MTAD[i],buffer,1000);
      //
#ifdef DEBUG
      Serial.println(buffer);
#endif
      if(strstr(buffer,MTAD[i]==NULL)){
        continue;
      }
      //prepare
      int wattHr,watt,CRC;
      int cs=0;
      sscanf(buffer,"%*s %*s %d %d %x",&wattHr,&watt,&CRC);
      for(int ics=0;ics<32;ics++){
        cs+=buffer[ics];
      }
      cs&=0xff;
      if(cs!=CRC){ // if checksum
        continue;
      }

#ifdef DEBUG
      Serial.print("Watt-Hour : ");
      Serial.println(wattHr);
      Serial.print("Watt-Hour : ");
      Serial.println(watt);
      Serial.print("CRC : ");

      Serial.println(cs,HEX);
      Serial.println(CRC,HEX);

#endif
    }
    //Serial.println("ATCN");
  }
}
int8_t enterCMDXbee(){
  int attemp=2;
  int answer=0;
  while(attemp>0){
    Serial.print("+++");
    delay(2000);
    answer = sendATXBee("AT","OK",1000);
    attemp--;
    if(answer==1)break;
    delay(2000);

  }
  return answer;
}

int8_t sendATXBee(char* ATcommand, char* expected_answer, unsigned int timeout){
  uint8_t x=0,  answer=0;
  char response[100];
  unsigned long previous;

  memset(response, '\0', 100);    // Initialize the string

  delay(100);

  while( Serial.available() > 0) Serial.read();    // Clean the input buffer
  Serial.println(ATcommand);    // Send the AT command 
  x = 0;
  previous = millis();
  do{
    if(Serial.available() != 0){    
      response[x] = Serial.read();
      //Serial.print(response[x]);
      x++;
      if (strstr(response, expected_answer) != NULL)    
      {
        //Serial.println("ANSWER : ");

        answer = 1;
      }
    }
  }
  while((answer == 0) && ((millis() - previous) < timeout));   
  return answer;
}

void GetMeter(char* cmd ,char *response, unsigned int timeout){
  //char response[100]="";
  uint8_t x=0,  answer=0;

  unsigned long previous;
  memset(response, '\0', 100);    // Initialize the string

  delay(100);
  while( Serial.available() > 0) Serial.read();    // Clean the input buffer
  Serial.println(cmd);    // Send the AT command 
  x = 0;
  previous = millis();
  do{
    if(Serial.available() != 0){    
      response[x] = Serial.read();
      //Serial.print(response[x]);
      if(response[x]=='\n')answer=1;
      x++;
    }
  }
  while((answer == 0) && ((millis() - previous) < timeout));  
  // return response;
}

void reffunc(char *list){
  sprintf(list,"Ref OK");
}






