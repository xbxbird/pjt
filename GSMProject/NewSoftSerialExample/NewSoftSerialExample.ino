#include <NewSoftSerial.h>

NewSoftSerial mySerial(2, 3);

void setup()  
{
  Serial.begin(9600);
  Serial.println("Goodnight moon!");

  // set the data rate for the SoftwareSerial port
  mySerial.begin(19200);
  mySerial.println("AT");
}

void loop() // run over and over
{
  if (mySerial.available())
    Serial.print((char)mySerial.read());
  if (Serial.available())
    mySerial.print((char)Serial.read());
}
