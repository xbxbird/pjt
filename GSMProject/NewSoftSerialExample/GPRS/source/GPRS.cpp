#include "GPRS_main.h"

#include <SoftwareSerial.h>
SoftwareSerial gsmSerial(2, 3); // RX, TX

void setup()  
{
  // Serial Communication
  Serial.begin(9600);
  
  Serial.println("START");

  // GsmSerial SoftwareSerial port
  gsmSerial.begin(19200);
  gsmSerial.println("AT\r\n");
  Serial.println("GSM OK");
  Serial.println("GSM OK");
  Serial.println("GPRS OK");
  Serial.println("Connecting 202.44.37.25/testpost.php");
  Serial.println("Post OK");
  Serial.println("GPRS Error");
  Serial.println("GSM OK");
  Serial.println("GPRS Error");
  Serial.println("GSM OK");
  Serial.println("GSM Error");
  S

}

void loop() // run over and over
{
  if (gsmSerial.available())
    Serial.write(gsmSerial.read());
  //if (Serial.available())
  //gsmSerial.write(Serial.read());
  checkSheild();

}
boolean checkSheild(){
  if(sendATcommand("AT","OK",500)){ /// Test with send AT
    Serial.println("GSM OK");
    return true;
  }
  else{
    Serial.println("GSM Error");  /// if not Receive OK answer
    return false;
  }
}
int8_t sendATcommand(char* ATcommand, char* expected_answer, unsigned int timeout){

  uint8_t x=0,  answer=0;
  char response[100];
  unsigned long previous;

  memset(response, '\0', 100);    // Initialize the string

  delay(100);

  while( gsmSerial.available() > 0) gsmSerial.read();    // Clean the input buffer
  gsmSerial.println(ATcommand);    // Send the AT command 
  x = 0;
  previous = millis();

  // this loop waits for the answer
  do{
    if(gsmSerial.available() != 0){    
      // if there are data in the UART input buffer, reads it and checks for the asnwer
      response[x] = gsmSerial.read();
      x++;
      // check if the desired answer  is in the response of the module
      if (strstr(response, expected_answer) != NULL)    
      {
        //Serial.println("ANSWER : ");
        //Serial.println(response);
        answer = 1;
      }
    }
    // Waits for the asnwer with time out
  }
  while((answer == 0) && ((millis() - previous) < timeout));   

  return answer;
}

