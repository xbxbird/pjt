#include "xbee_main.h"

#define DEBUG 1

int led = 13;
void setup() {
  // initialize serial:
  pinMode(led, OUTPUT);
  Serial.begin(1230);
}

void loop() {
  // print the string when a newline arrives:
  ///answer = sendATXBee("+++","OK",500);


  //Serial.print("AT\n");
  delay(1000); 
  if(enterCMDXbee()==1){
    while(1){
#ifdef DEBUG
      Serial.println("Enter command mode!!");
#endif  
      digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
      delay(1000);               // wait for a second
      digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW
      delay(1000);     
    }
  }
}

int8_t enterCMDXbee(){
  Serial.print("+++");
  delay(2000);
  int answer = sendATXBee("AT","OK",1000);
  return answer;
}
int8_t sendATXBee(char* ATcommand, char* expected_answer, unsigned int timeout){
  uint8_t x=0,  answer=0;
  char response[100];
  unsigned long previous;

  memset(response, '\0', 100);    // Initialize the string

  delay(100);

  while( Serial.available() > 0) Serial.read();    // Clean the input buffer
  Serial.println(ATcommand);    // Send the AT command 
  x = 0;
  previous = millis();

  // this loop waits for the answer
  do{
    if(Serial.available() != 0){    
      // if there are data in the UART input buffer, reads it and checks for the asnwer
      response[x] = Serial.read();
      Serial.print(response[x]);
      x++;
      // check if the desired answer  is in the response of the module
      if (strstr(response, expected_answer) != NULL)    
      {
        //Serial.println("ANSWER : ");

        answer = 1;
      }
    }
    // Waits for the asnwer with time out
  }
  while((answer == 0) && ((millis() - previous) < timeout));   
  return answer;
}



